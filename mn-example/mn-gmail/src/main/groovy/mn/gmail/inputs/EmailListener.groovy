package mn.gmail.inputs

import io.micronaut.configuration.rabbitmq.annotation.Queue
import io.micronaut.configuration.rabbitmq.annotation.RabbitListener
import mn.gmail.EmailBean
import mn.gmail.service.EmailService

@RabbitListener
class EmailListener {

    EmailService emailService

    EmailListener(EmailService emailService){
        this.emailService = emailService
    }

    @Queue('notifications')
    void receive(EmailBean emailBean){
        emailService.send(emailBean)
    }

}
