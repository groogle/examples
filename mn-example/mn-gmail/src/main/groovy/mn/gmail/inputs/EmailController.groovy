package mn.gmail.inputs

import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.validation.Validated
import mn.gmail.EmailBean
import mn.gmail.EmailClient
import mn.gmail.service.EmailService

import javax.validation.Valid

@Controller
@Validated
class EmailController {

    EmailClient emailClient

    EmailController( EmailClient emailClient){
        this.emailClient = emailClient
    }

    @Post
    HttpStatus send( @Body @Valid EmailBean emailBean){

        emailClient.send( emailBean )

        HttpStatus.OK
    }

}
