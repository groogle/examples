package mn.gmail.service

import com.google.api.services.gmail.GmailScopes
import com.puravida.groogle.GmailService
import com.puravida.groogle.GmailServiceBuilder
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import io.micronaut.context.annotation.ConfigurationProperties

import javax.annotation.PostConstruct
import javax.validation.constraints.NotBlank

@ConfigurationProperties('groogle')
class GroogleConfiguration {

    @NotBlank
    String credentials

    @NotBlank
    String accountUser

    Groogle groogle

    @PostConstruct
    void init(){
        GroogleConfiguration self = this
        groogle = GroogleBuilder.build {    //<1>
            withServiceCredentials {
                withScopes GmailScopes.MAIL_GOOGLE_COM
                usingCredentials self.credentials
                accountUser self.accountUser
            }
            service(GmailServiceBuilder.build(), GmailService)  //<2>
        }
    }

    GmailService getGmailService(){
        groogle.service(GmailService)
    }

}
