package mn.gmail.service

import com.puravida.groogle.GmailService
import mn.gmail.EmailBean
import javax.inject.Singleton

@Singleton
class EmailService {

    GmailService service

    EmailService(GroogleConfiguration groogleConfiguration){
        service = groogleConfiguration.gmailService
    }

    void send(EmailBean bean){
        service.sendEmail {
            from 'me'
            to bean.to
            subject bean.subject
            body bean.body
        }
    }

}
