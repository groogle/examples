package mn.gmail

import io.micronaut.configuration.rabbitmq.annotation.Binding
import io.micronaut.configuration.rabbitmq.annotation.RabbitClient

@RabbitClient
interface EmailClient {

    @Binding('notifications')
    void send( EmailBean bean )

}
