package mn.gmail

import io.micronaut.core.annotation.Introspected

import javax.validation.constraints.NotBlank

@Introspected
class EmailBean {

    @NotBlank
    String to

    @NotBlank
    String subject

    @NotBlank
    String body

}
