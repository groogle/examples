package mn.gmail

import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class EmailControllerBadParamsSpec extends Specification{

    @AutoCleanup
    @Shared
    EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer)

    @AutoCleanup
    @Shared
    HttpClient httpClient = embeddedServer.applicationContext.createBean(HttpClient, embeddedServer.URL)

    BlockingHttpClient getClient(){
        httpClient.toBlocking()
    }

    def "no hay get"(){

        when:

        client.exchange(HttpRequest.GET('/'))

        then:

        HttpClientResponseException e = thrown()

        and:

        e.response.status == HttpStatus.METHOD_NOT_ALLOWED
    }

}
