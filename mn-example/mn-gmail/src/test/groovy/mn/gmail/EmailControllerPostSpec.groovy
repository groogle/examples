package mn.gmail
import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy

//import org.testcontainers.containers.GenericContainer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class EmailControllerPostSpec extends Specification{

    static final String RABBITMQ_TEST_ROUTING_KEY = "notifications";
    static final int RABBITMQ_PORT = 5672;

    static
    GenericContainer rabbitContainer =
            new GenericContainer("library/rabbitmq:3.7")
                    .withExposedPorts(RABBITMQ_PORT, 15672)
                    .withEnv([
                            "RABBITMQ_DEFAULT_USER":'rabbitmq',
                            "RABBITMQ_DEFAULT_PASS":'rabbitmq',
                    ])
                    .waitingFor(new LogMessageWaitStrategy().withRegEx("(?s).*Server startup complete.*"))

    static {
        rabbitContainer.start()
        int port = rabbitContainer.getMappedPort(15672)
        println port
    }

    @AutoCleanup
    @Shared
    EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer,
            ["rabbitmq.uri": "amqp://localhost:"+rabbitContainer.getMappedPort(5672)])

    @AutoCleanup
    @Shared
    HttpClient httpClient = embeddedServer.applicationContext.createBean(HttpClient, embeddedServer.URL)


    BlockingHttpClient getClient(){
        httpClient.toBlocking()
    }

    def "empty post"(){

        when:

        client.exchange(HttpRequest.POST('/',[:]))

        then:

        HttpClientResponseException e = thrown()

        and:

        e.response.status == HttpStatus.BAD_REQUEST

    }


    def "valid post"(){

        when:

        client.exchange(HttpRequest.POST('/',[to:'groogle-groovy@gmail.com',subject:'hola',body:'caracola']))

        then:

        noExceptionThrown()
    }


    def "a huge valid post"(){

        when:

        (0..1000).each {
            client.exchange(HttpRequest.POST('/', [to: 'groogle-groovy@gmail.com', subject: 'hola', body: 'caracola']))
        }
        then:

        noExceptionThrown()
    }

}
