package mn.gmail

import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class EmailRabbitSpec extends Specification{

    static final String RABBITMQ_TEST_ROUTING_KEY = "notifications";
    static final int RABBITMQ_PORT = 5672;

    static
    GenericContainer rabbitContainer =
            new GenericContainer("library/rabbitmq:3.7")
                    .withExposedPorts(RABBITMQ_PORT, 15672)
                    .withEnv([
                            "RABBITMQ_DEFAULT_USER":'rabbitmq',
                            "RABBITMQ_DEFAULT_PASS":'rabbitmq',
                    ])
                    .waitingFor(new LogMessageWaitStrategy().withRegEx("(?s).*Server startup complete.*"))

    static {
        rabbitContainer.start()
        int port = rabbitContainer.getMappedPort(15672)
        println port
    }

    //tag::startup[]
    @AutoCleanup
    @Shared
    EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer,
            ["rabbitmq.uri": "amqp://localhost:"+rabbitContainer.getMappedPort(5672)])

    @Shared
    EmailClient emailClient = embeddedServer.applicationContext.createBean(EmailClient)
    //end::startup[]

    //tag::send[]
    def "send an email with #subject via rabbit"(String to, String subject, String body){
        expect:
        EmailBean emailBean = new EmailBean(to: to, subject: subject, body: body)
        emailClient.send(emailBean)
        true

        where:
        to | subject | body
        'groovy.groogle@gmail.com' | 'hola' | 'caracola'
    }
    //end::send[]
}
