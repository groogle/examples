package mn.gmail

import io.micronaut.context.ApplicationContext
import mn.gmail.inputs.EmailController
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class EmailControllerSpec extends Specification{

    @AutoCleanup
    @Shared
    ApplicationContext applicationContext = ApplicationContext.run()


    def "test email controller exist"(){

        expect:

        applicationContext.containsBean(EmailController)

        and:

        !applicationContext.containsBean(EmailBean)
    }


}
