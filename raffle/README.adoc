= Raffle

Proyecto para crear un sorteo sobre la lista de asistentes en una hoja de GoogleSheet

== Requisitos

Java 11 + JavaFX (por ejemplo el Java 8 de Oracle, o en su defecto una implementacion
configurada con la variable de entorno JAVAFX_HOME)

== Preparación

Crear una hoja de GoogleSheet y añadir a los asistentes siguiendo el siguiente formato:

- la primera fila contiene la configuración del Meetup, tal que:
* la primera columna es la url al logo del Meetup y sirve para configurar el icono. Si se deja en blanco se usará
el icono por defecto
* la segunda columna es el título del Meetup a mostrar en la parte superior. Si se deja en blanco se usará
un texto por defecto
* la tercera columna contiene un breve mensaje a enviar en el body del correo al ganador. Si se deja en blanco
no se envia email aunque el participante lo proporcione

- la segunda fila contiene las cabeceras y no será interpretada

- la primera columna contiene el nombre a mostrar
- la segunda columna contiene el email (si se deja en blanco no se envia obviamente)
- en la tercera columna se escribirá la fecha y hora del ganador
- el resto de las columnas son ignoradas pero es fácil añadir otros usos

Obtener unas credenciales de cliente desde la consola de proyectos de Google https://console.cloud.google.com
y descargar el json en el directorio principal del proyecto

== Build

Descargar o clonar el proyecto. El proyecto contiene una serie de ejemplos encontrándose este en el directorio
`raffle`, el cual asumiremos como directorio raiz para este ejemplo.

crear un fichero `gradle.properties` con los siguientes valores:

[source]
----
sheet=1j5xxxAAAbbbddcccddd
tab=test
credentials=client_secret.json
----

Donde `sheet` es el ID de la hoja donde tenemos los meetups y `tab` es el nombre del tab que queremos usar.
La property `credentials` por su parte es una ruta absoluta o relativa al directorio raiz donde se encuentra
el fichero obtenido de la consola Google


Si lo ejecutamos con `gradle` todos los parámetros pueden ser proporcionados como argumentos `-P` como se muestra
a continuación


== Ejecutar

[source]
----
./gradle run
----

como argumentos proporcionaremos el id de la hoja que se puede obtener de la URL cuando estamos
editandola con el navegador, y el nombre del tab a usar (así puedes tener más de una lista en el Sheet)

como tercer argumento puedes indicar la ruta al fichero de credenciales, siendo `client_secret.json` tomada por defecto


[source]
----
./gradlew run -Ptab=MadridGug -Pcredentials=../../../../../raffle_credentials.json -Psheet=addsdfsdf12321312
----

== Autentificacion

La primera vez que se ejecute el programa se abrirá un navegador para que el organizador proporcione
las credenciales (se identifique) necesarias para poder leer la hoja indicada y ya no se volverán a pedir.

Si el organizador no quiere persistir sus credenciales
puede hacerlo modificando simplemente el fichero `src/main/groovy/raffle/RaffleService.groovy`
cambiando `storeCredentials true` a `storeCredentials false`. De esta forma el sistema puede guardar
las credenciales proporcionadas en el directorio $HOME/.credentials/raffle. Simplemente borrando este
directorio el programa volverá a solicitar autentificación cuando se vuelva a ejecutar.

== Procedimiento

Si todo va bien, el programa al arrancar cargará la lista de asistentes y la mostrará desordenada.

El organizador podrá entonces pulsar `Sortear` y el sistema seleccionará un item de la lista de forma
aleatoria mostrando su nombre en un diálogo.

Si el asistente se encuentra y acepta el regalo simplemente pulsaremos `aceptar`. Si por el contrario el 
asistente no está o cede su turno pulsaremos `cancelar` y el sistema le eliminará de la lista, volviendo
a desordenar la lista permitiendo al organizador volver a pulsar `sortear`
